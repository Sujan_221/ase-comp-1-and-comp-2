﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;

namespace GraphicalProgramSujan
{
    /// <summary>
    /// Summary description for TestingRectangle
    /// </summary>
    [TestClass]
    public class TestingRectangle
    {
        int text;
        Brush mybrush;
        Color mypen = Color.Black;

        public TestingRectangle()
        {

        }

        private TestContext testContext;
        public TestContext TestContext
        {
            get
            {
                return testContext;
            }
            set
            {
                testContext = value;
            }
        }
        [TestMethod]
        public void TestMethod1()
        {
            //
            // add test logic here
            //
            var Rectangle = new GraphicalProgramSujan.Rectangle();
            int x = 200, y = 200, width = 100, height = 200;
            Brush mybrush1 = mybrush;
            Rectangle.set(
                text, mybrush1, mypen, x, y, width, height);
            Assert.AreEqual(200, Rectangle.x);
        }
    }
}
